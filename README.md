# About this fork

This fork is made for The Commonwealth server. It is improved and changed in some ways.
Changes are:
 - Use <a href="https://gitlab.com/tyoda-wurm/PiggyBank">Piggy Banks</a> to give the rewarded coins in chests
 - Configure if chests should disappear when digger is X tiles away
 - Fixes to bugs such as infinite loops
# TreasureHunting
PVEpLands Treasure hunting mod for Wurm Unlimited, inspired by Ultima Online

https://forum.wurmonline.com/index.php?/topic/157184-released-pveplands-treasure-hunting-inspired-by-ultima-online-treasure-maps/
