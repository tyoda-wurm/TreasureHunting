package com.wurmonline.server.zones;

import com.wurmonline.server.items.Item;

public class TreasureVolaTileHelper {
    public static void hideItem(Item item){
        VolaTile tile = Zones.getOrCreateTile(item.getTileX(), item.getTileY(), item.isOnSurface());
        if(tile != null) {
            tile.removeItem(item, false);
        }
    }

    public static void unHideItem(Item hiddenItem){
        VolaTile tile = Zones.getOrCreateTile(hiddenItem.getTileX(), hiddenItem.getTileY(), hiddenItem.isOnSurface());
        if(tile != null) {
            tile.addItem(hiddenItem, false, false);
        }
    }
}
