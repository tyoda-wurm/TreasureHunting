package com.pveplands.treasurehunting;

import com.wurmonline.server.Items;
import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.sounds.SoundPlayer;
import com.wurmonline.server.zones.Zone;
import com.wurmonline.server.zones.Zones;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gotti.wurmunlimited.modsupport.actions.ActionPerformer;
import org.gotti.wurmunlimited.modsupport.actions.ModAction;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;
import org.jetbrains.annotations.NotNull;

/**
 * Game master feature to spawn a treasure chest.
 */
public class SpawnTreasureChestAction implements ActionPerformer, ModAction {
    private static final Logger logger = Logger.getLogger(TreasureHunting.getLoggerName(SpawnTreasureChestAction.class));

    private final short actionId;
    private final ActionEntry actionEntry;

    public SpawnTreasureChestAction() {
        actionId = (short)ModActions.getNextActionId();
        actionEntry = ActionEntry.createEntry(actionId, "Spawn treasure chest HERE", "spawning treasure chest", MiscConstants.EMPTY_INT_ARRAY);
        ModActions.registerAction(actionEntry);
    }

    @Override
    public boolean action(@NotNull Action action, @NotNull Creature performer, @NotNull Item source, int tilex, int tiley, boolean onSurface, int heightOffset, int tile, short num, float counter) {
        if (!(performer instanceof Player)) return true;

        if (performer.getPower() <= 1) {
            Logger.getLogger(TreasureHunting.getLoggerName(CreateRandomTreasuremapAction.class))
                    .warning(String.format("%s tried to spawn a treasure chest, this might well fall under exploiting.", performer));

            return true;
        }

        logger.info(String.format("%s is spawning a %.2f quality treasure chest.", performer.getName(), source.getCurrentQualityLevel()));

        Item treasuremap = Treasuremap.CreateTreasuremap(performer, null, null, null, true);

        if (treasuremap == null) {
            performer.getCommunicator().sendNormalServerMessage("Treasuremap dummy creation failed, probably couldn't find a suitable spot. Try again.");
            logger.info("Treasuremap dummy creation failed, probably couldn't find a suitable spot. Try again.");
            performer.getLogger().info("Treasuremap dummy creation failed, probably couldn't find a suitable spot. Try again.");
        }
        else {
            treasuremap.setDataXY(tilex, tiley);

            float quality;

            if (source.getAuxData() < 1 || source.getAuxData() > 100)
                quality = new Random().nextFloat() * 100;
            else
                quality = source.getAuxData();

            treasuremap.setQualityLevel(Math.min(100, Math.max(1, quality)));
            TreasureChest treasureChest;
            try {
                treasureChest = new TreasureChest(performer, treasuremap);
            }catch(ChestNotCreatedException e){
                performer.getCommunicator().sendNormalServerMessage("Treasure chest could not be created.");
                performer.getLogger().warning("Treasure chest could not be created.");
                logger.warning("Treasure chest could not be created.");
                return true;
            }
            Item chestItem = treasureChest.getChestItem();

            Items.destroyItem(treasuremap.getWurmId());

            Random random = new Random();
            Item lock = null;

            try {
                if (random.nextFloat() <= TreasureHunting.getOptions().getLockChance() / 100f) {
                    float lockQuality = chestItem.getCurrentQualityLevel() * TreasureHunting.getOptions().getLockMultiplier();

                    // large padlock.
                    lock = ItemFactory.createItem(194, lockQuality, null);
                    chestItem.setLockId(lock.getWurmId());
                    chestItem.lock();
                    lock.lock();

                    SoundPlayer.playSound("sound.object.lockunlock", chestItem.getTileX(), chestItem.getTileY(), true, 1.0f);
                    performer.getCommunicator().sendNormalServerMessage(String.format("Chest was locked with a lock of %.2f quality.", lockQuality));
                    logger.log(Level.INFO, "Chest was locked with a lock of {0} quality.", lockQuality);
                }
                else {
                    performer.getCommunicator().sendNormalServerMessage("Chest was not locked.");
                    logger.info("Chest was not locked.");
                }

                Zone zone = Zones.getZone(tilex, tiley, true);
                chestItem.setPos((tilex << 2) + 2, (tiley << 2) + 2, performer.getPositionZ(), performer.getStatus().getRotation(), performer.getBridgeId());
                zone.addItem(chestItem);
            }
            catch (Exception e) {
                logger.log(Level.SEVERE, "Chest locking failed.", e);
                performer.getCommunicator().sendAlertServerMessage("Error creating chest lock, check server console/logfile.");

                if (lock != null)
                    Items.destroyItem(lock.getWurmId());
            }
        }

        return true;
    }

    @Override
    public short getActionId() {
        return actionId;
    }

    public ActionEntry getActionEntry() {
        return actionEntry;
    }
}
