package com.pveplands.treasurehunting;

public class ChestNotCreatedException extends RuntimeException {
    public ChestNotCreatedException(){ super(); }
    public ChestNotCreatedException(String message){ super(message); }
    public ChestNotCreatedException(String message, Exception e){ super(message, e); }
}
