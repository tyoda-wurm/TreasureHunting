package com.pveplands.treasurehunting;

import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import org.gotti.wurmunlimited.modsupport.actions.ActionPerformer;
import org.gotti.wurmunlimited.modsupport.actions.ModAction;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Logger;

/**
 * Action to enable staff to teleport to the treasure map's target coordinates.
 */
public class PoofTreasureChestAction implements ActionPerformer, ModAction {
    public static final Logger logger = Logger.getLogger(TreasureHunting.getLoggerName(CreateRandomTreasuremapAction.class));
    private final short actionId;
    private final ActionEntry actionEntry;

    public PoofTreasureChestAction() {
        actionId = (short)ModActions.getNextActionId();
        actionEntry = ActionEntry.createEntry(actionId, "poof treasure chest", "poofing treasure chest", new int[0]);
        ModActions.registerAction(actionEntry);
    }

    @Override
    public short getActionId() {
        return actionId;
    }

    public ActionEntry getActionEntry(){
        return actionEntry;
    }

    @Override
    public boolean action(@NotNull Action action, @NotNull Creature performer, @NotNull Item source, @NotNull Item target, short num, float counter) {
        return performMyAction(performer, target);
    }

    @Override
    public boolean action(@NotNull Action action, @NotNull Creature performer, @NotNull Item target, short num, float counter) {
        return performMyAction(performer, target);
    }

    private boolean performMyAction(Creature performer, Item target) {
        if(target.getTemplateId() != ItemList.treasureChest) return true;

        if (performer.getPower() <= 1) {
            logger.warning(String.format("%s tried to poof a treasure chest.", performer));

            return true;
        }

        TreasureChest treasureChest = TreasureChest.getTreasureChestForChestId(target.getWurmId());

        if(treasureChest == null){
            logger.info("Treasure chest was null for chest item.");
            return true;
        }

        treasureChest.poof();

        return true;
    }
}
