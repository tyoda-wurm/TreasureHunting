package com.pveplands.treasurehunting;

import com.wurmonline.server.Items;
import com.wurmonline.server.Server;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;

import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.actions.ActionPerformer;
import org.gotti.wurmunlimited.modsupport.actions.BehaviourProvider;
import org.gotti.wurmunlimited.modsupport.actions.ModAction;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;

import java.util.Collections;
import java.util.List;

public class CombineTreasuremapAction implements ModAction, ActionPerformer, BehaviourProvider {
    private final short actionId = (short) ModActions.getNextActionId();
    private final ActionEntry actionEntry;

    public CombineTreasuremapAction() {
        this.actionEntry = ActionEntry.createEntry(this.actionId, "Combine", "combining", new int[]{Actions.ACTION_TYPE_IGNORERANGE});
        ModActions.registerAction(this.actionEntry);
    }

    @Override
    public short getActionId() {
        return this.actionId;
    }

    public ActionEntry getActionEntry() {
        return this.actionEntry;
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item activated, Item target) {
        TreasureOptions options = TreasureHunting.getOptions();
        int treasureMapId = options.getTreasuremapTemplateId();
        if(performer == null || !performer.isPlayer()
                || !options.isAllowCombineAction()
                || activated.getTemplateId() != treasureMapId
                || target.getTemplateId() != treasureMapId
                || activated.getParentId() == -10
                || target.getParentId() == -10
                || activated == target)
            return null;
        return Collections.singletonList(actionEntry);
    }

    public boolean action(Action action, Creature performer, Item source, Item target, short num, float counter) {
        if (counter == 1.0f) {
            TreasureOptions options = TreasureHunting.getOptions();
            int treasureMapId = options.getTreasuremapTemplateId();
            if(target.getTemplateId() != treasureMapId
                    || source.getTemplateId() != treasureMapId
                    || !options.isAllowCombineAction()
                    || source == target)
                return true;
            if(target.getOwnerId() != performer.getWurmId()
                    || source.getOwnerId() != performer.getWurmId()) {
                performer.getCommunicator().sendNormalServerMessage("You may not combine those two maps right now.");
                return true;
            }

            int tenthOfSeconds = TreasureHunting.getOptions().getCombineActionTimeTenthOfSeconds();
            action.setTimeLeft(tenthOfSeconds);
            performer.sendActionControl("Combining maps", true, tenthOfSeconds);
            performer.getCommunicator().sendNormalServerMessage("You start to combine the two maps.");
            Server.getInstance().broadCastAction(performer.getName() + " starts to combine two maps.", performer, 5);
        }
        else {
            if(counter*10.0f > action.getTimeLeft()) {
                float average = (source.getCurrentQualityLevel() + target.getCurrentQualityLevel()) / 2.0f;
                try {
                    ReflectionUtil.setPrivateField(source, ReflectionUtil.getField(Item.class, "damage"), 0.0f);
                } catch (IllegalArgumentException | IllegalAccessException | ClassCastException | NoSuchFieldException e) {
                    throw new RuntimeException(e);
                }
                source.setQualityLevel(average + 10.0f);
                Treasuremap.setRandomSpot(source, performer.getTileX(), performer.getTileY());
                Items.destroyItem(target.getWurmId(), false, true);
                performer.getCommunicator().sendNormalServerMessage("You created a new map that points to more treasure!");
                return true;
            }
        }
        return false;
    }
}
