package com.pveplands.treasurehunting;

import com.wurmonline.server.*;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.Creatures;
import com.wurmonline.server.creatures.NoSuchCreatureException;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.NoSuchTemplateException;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.zones.TreasureVolaTileHelper;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class to spawn a treasure chest with all its contents.
 */
public class TreasureChest {
    private static final Logger logger = Logger.getLogger(TreasureHunting.getLoggerName(TreasureChest.class));

    private static final ArrayList<TreasureChest> treasureChests = new ArrayList<>();

    private final HashSet<Long> guardians = new HashSet<>();
    private final ArrayList<Item> hiddenItems = new ArrayList<>();
    private final Player digger;
    private final Item chestItem;

    private final float mapQL;
    private final float mapDMG;
    private final byte mapRarity;
    private boolean isPoofing = false;

    /**
     * Generates a treasure chest with all contents from a treasure map.
     * @param performer Creatures the treasure is generated for.
     * @param map Treasure map being used.
     * @throws ChestNotCreatedException if performer is not Player or map is null or not a treasure map.
     */
    public TreasureChest(Creature performer, Item map) throws ChestNotCreatedException {
        if (!(performer instanceof Player)) throw new ChestNotCreatedException("Could not create chest. Digger was not a player.");

        digger = (Player)performer;

        if (map == null || map.getTemplateId() != TreasureHunting.getOptions().getTreasuremapTemplateId()) {
            throw new ChestNotCreatedException("Could not create chest. Tried to generate treasure chest for an item that is not a treasure map or null.");
        }

        mapQL = map.getQualityLevel();
        mapDMG = map.getDamage();
        mapRarity = map.getRarity();

        try {
            double quality = Math.max(1, Math.min(100d, map.getCurrentQualityLevel() + ((100-map.getCurrentQualityLevel()) * (map.getRarity() * 0.25f))));

            logger.info("Creating treasure chest item.");
            chestItem = ItemFactory.createItem(ItemList.treasureChest, (float)quality, map.getRarity(), null);

            logger.info("Creating money reward.");
            addItems(TreasureReward.getMoney(quality));
            logger.info("Creating sleep powder reward.");
            addItems(TreasureReward.getSleepPowder(quality));
            logger.info("Creating precious metals.");
            addItem(TreasureReward.getPreciousMetal(quality));
            logger.info("Creating HOTA reward");
            addItem(TreasureReward.getHotaStatue(quality));
            logger.info("Creating rare item reward.");
            addItem(TreasureReward.getRareItem(performer, quality));
            logger.info("Creating very rare item reward.");
            addItem(TreasureReward.getVeryRareItem(quality));
            logger.info("Creating extremely rare item reward.");
            addItem(TreasureReward.getExtremelyRareItem(quality));
            logger.info("Creating unfinished (kingdom) item reward.");
            addItem(TreasureReward.getUnfinishedItem(performer, quality));
            logger.info("Creating actual item reward.");
            addItems(TreasureReward.getTierRewards(quality));
            logger.info("Creating karma reward.");
            addItem(TreasureReward.getKarmaReward(quality));
        }
        catch (Exception e) {
            throw new ChestNotCreatedException("Could not create chest. "+e.getMessage(), e);
        }

        treasureChests.add(this);
    }

    /**
     * Destroys the treasure chest along with all its contents and guardians.
     * Creates a new map pointing to the same location and with the same quality as the chest.
     * Gives map to player.
     */
    public void poof(){
        isPoofing = true;

        logger.info("Chest is getting poofed. Player:"+digger.getName()+" Pos:"+chestItem.getTileX()+","+chestItem.getTileY());

        //destroy items inside and chest
        destroyItemsRecursive(chestItem);

        // poof hidden items
        for(Item hiddenItem : hiddenItems){
            destroyItemsRecursive(hiddenItem);
        }

        // poof guardians
        for (long creatureId : guardians.toArray(new Long[0])) {
            try {
                Creature creature = Creatures.getInstance().getCreature(creatureId);
                for (Item item : creature.getInventory().getAllItems(true)) {
                    destroyItemsRecursive(item);
                }
                creature.die(true, "got poofed", true);
            } catch (NoSuchCreatureException e) {
                logger.warning("Tried to destroy guardian, but it does not exist. id: " + creatureId);
            }
        }

        Server.getInstance().broadCastMessage("The chest and its guardians disappear", chestItem.getTileX(), chestItem.getTileY(), true, 2);

        if(mapDMG + TreasureHunting.getOptions().getDamageOnPoof() < 100f) {
            Item map;
            try {
                map = ItemFactory.createItem(TreasureHunting.getOptions().getTreasuremapTemplateId(), mapQL, mapRarity, null);
                map.setDamage(mapDMG+TreasureHunting.getOptions().getDamageOnPoof());
            }catch(FailedException | NoSuchTemplateException e){
                logger.log(Level.SEVERE, "Creating map while poofing failed: "+e.getMessage(), e);
                return;
            }

            // copied from Treasuremap.CreateTreasuremap
            // This sets the Data1 value to (x << 16) | y.
            map.setDataXY(chestItem.getTileX(), chestItem.getTileY());

            // Vanilla behaviour, for items less than QL 1, it'll set it to 1.00
            // and puts half of it as damage.
            if (map.getQualityLevel() < 1.0) map.setDamage(-map.getQualityLevel() / 2f);

            map.setData2(Servers.localServer.id);

            digger.getInventory().insertItem(map, true, false, false);

            digger.getCommunicator().sendNormalServerMessage("A map appears in your inventory.");
        } else{
            digger.getCommunicator().sendNormalServerMessage("A map appears in your inventory but it's unreadable and crumbles to dust. You chuck it.");
        }

        treasureChests.remove(this);
    }

    /**
     * Returns the treasure chest associated with a guardian
     * @param wurmId The guardian's ID
     * @return The treasure chest the guardian belongs to, or null if none was found
     */
    @Nullable
    public static TreasureChest getTreasureChestForGuardian(Long wurmId){
        for(TreasureChest treasureChest : treasureChests){
            if(treasureChest.hasGuardian(wurmId))
                return treasureChest;
        }
        return null;
    }

    @Nullable
    public static TreasureChest getTreasureChestForChestId(long chestToFind){
        for(TreasureChest treasureChest : treasureChests){
            if(treasureChest.getChestItem().getWurmId() == chestToFind)
                return treasureChest;
        }
        return null;
    }

    /**
     * @param wurmId The guardian's ID
     * @return true if the guardian is associated with this chest
     */
    public boolean hasGuardian(long wurmId){
        return guardians.contains(wurmId);
    }

    /**
     * Registers a new guardian for the chest
     * @param wurmId The ID of the new guardian
     */
    public void addGuardian(long wurmId){
        guardians.add(wurmId);
    }

    /**
     * Removes a guardian from the chest, if present. Unlocks the chest if no more guardians are left
     * @param wurmId the guardian to remove
     * @return true if a guardian was removed, false otherwise
     */
    public boolean removeGuardian(long wurmId){
        boolean removed = guardians.remove(wurmId);
        logger.info("Removing guardian "+wurmId+" from chest "+chestItem.getTileX()+","+chestItem.getTileY()+". Was present:"+removed+" Remaining:"+guardians.size());

        if (guardians.size() == 0 && !isPoofing){
            try {
                if(chestItem.isLocked()){
                    Item lock = Items.getItem(chestItem.getLockId());
                    Server.getInstance().broadCastAction("The treasure's guardians have been defeated and the protection is reduced.", digger, 20);
                    if(TreasureHunting.getOptions().isDestroyLock()){
                        chestItem.unlock();
                        chestItem.setLockId(-10);
                        logger.info("Treasure unlocked at "+chestItem.getTileX()+","+chestItem.getTileY());
                        Items.destroyItem(lock.getWurmId(), true, false);
                        treasureChests.remove(this);
                        for(Item hiddenItem : hiddenItems){
                            TreasureVolaTileHelper.unHideItem(hiddenItem);
                        }
                        hiddenItems.clear();
                    }else{
                        lock.setQualityLevel(Math.max(1, lock.getQualityLevel()*0.3f));}
                }
            } catch (NoSuchItemException e) {
                logger.log(Level.WARNING, "Exception while getting lock item: ", e);
            }
        }

        return removed;
    }

    /**
     * @return the chest item belonging to this object
     */
    public Item getChestItem(){
        return chestItem;
    }

    /**
     * @return the player who dug up the treasure chest
     */
    public Player getDigger(){
        return digger;
    }

    public void addItem(@Nullable Item item) {
        addItem(item, false);
    }

    /**
     * Inserts an item into the chest.
     * @param item Item to be inserted, may be null.
     */
    public void addItem(@Nullable Item item, boolean isPlaced) {
        if (item == null)
            return;
        
        logger.log(Level.INFO, "Inserting {0} into treasure chest.", item.getName());
        chestItem.insertItem(item, true, false, isPlaced);
    }
    
    /**
     * Inserts a list of items into the chest.
     * @param list List of items, may be null or empty.
     */
    public void addItems(@Nullable ArrayList<Item> list) {
        if (list == null)
            return;
        
        for (Item item : list) {
            logger.log(Level.INFO, "Inserting {0} into treasure chest.", item.getName());
            chestItem.insertItem(item, true);
        }
    }

    public void addHiddenItem(@Nullable Item item){
        if(item == null) return;

        hiddenItems.add(item);

        TreasureVolaTileHelper.hideItem(item);
    }

    public static void onPlayerLogout(Player player){
        TreasureChest chestToPoof = null;
        for(TreasureChest treasureChest : treasureChests) {
            if (treasureChest.getDigger().getWurmId() == player.getWurmId()) {
                chestToPoof = treasureChest;
                break;
            }
        }

        if(chestToPoof != null)
            chestToPoof.poof();
    }

    public static void onServerShutdown(){
        while(!treasureChests.isEmpty())
            treasureChests.get(0).poof();
    }

    public static void onServerPoll(){
        if(TreasureHunting.getOptions().isPoofIfDiggerTooFar()) {
            for (int i =0; i < treasureChests.size(); ++i) {
                if(treasureChests.get(i).poofIfDiggerTooFar()) --i;
            }
        }
    }

    public static void destroyItemsRecursive(Item inventory){
        if(inventory == null) return;

        for(Item item : inventory.getAllItems(true)){
            destroyItemsRecursive(item);
        }
        Items.destroyItem(inventory.getWurmId(), true, false);
    }

    public boolean poofIfDiggerTooFar(){
        int distance = Math.max(Math.abs(chestItem.getTileX() - digger.getTileX()),
                                Math.abs(chestItem.getTileY() - digger.getTileY()));

        if(distance > TreasureHunting.getOptions().getDiggerMaxDistanceTiles()){
            digger.getCommunicator().sendNormalServerMessage("You are too far from the treasure chest.");
            this.poof();
            return true;
        }
        return false;
    }
}
